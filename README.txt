﻿MetalRacer

MetalRacer es un juego de tipo Super Off Road. Conduces un tanque por distintos
circuítos de difícil orografía donde aparecen recompensas (para ganar puntos y dinero)
y Power-Ups para utilizar contra tus contrincantes. Hemos implementado dos modos de
juegos:

* Time-Attack: Tu objetivo es acabar la vuelta en el menor tiempo posible. Tienes 3 vueltas.

COMPILACIÓN E INSTALACIÓN

En el archivo comprimido se encuentra lo siguiente:
- MetalRacer -> contiene el código fuente, el archivo de configuración del proyecto
QtCreator y los archivos *.blend
- build-MetalRacer-Release -> contiene el ejecutable y el makefile

Para realizar una nueva compilación se debe respetar esta jerarquía de carpetas.

Dependendicas: 
OGRE, OIS, SDL_Mixer, Xerces, bullet, OgreBullet, CEGUI
Para su compilación se adjunta un Makefile distinto al sugerido durante el curso, ya
que, nosotros hemos realizado el proyecto utilizando qmake, que genera automáticamente
su propio Makefile.

REPOSITORIO PÚBLICO

https://bitbucket.org/Fergus1986/metalracer