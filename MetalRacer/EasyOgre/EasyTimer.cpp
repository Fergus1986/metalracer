#include "EasyTimer.h"

EasyTimer::EasyTimer()
{
}

EasyTimer::EasyTimer(int estado, int m, int s)
{
    this->estado = estado;
    this->minuto = m;
    this->segundo = s;
    this->tiempoActivo=true;
    tiempo = g_timer_new();
}

EasyTimer::~EasyTimer()
{
    this->pararTiempo();
     g_timer_destroy (tiempo);
}


bool EasyTimer::getTiempoActivo()
{
    if(this->estado==ADELANTE){
        if(this->getMinutosHaciaDelante()>=this->minuto &&
           this->getSegundosHaciaDelante() > this->segundo){
                this->tiempoActivo = false;

        }
    }
    else{
        this->getSegundosHaciaAtras();

    }
    return this->tiempoActivo;
}

int EasyTimer::getMinuto()
{
    if(this->estado==ADELANTE){
         return this->getMinutosHaciaDelante();
     }
     else{
         return this->getMinutosHaciaAtras();
    }
}

int EasyTimer::getSegundo()
{
    if(this->estado==ADELANTE){
        return this->getSegundosHaciaDelante();
    }
    else{
        return this->getSegundosHaciaAtras();
    }
}

void EasyTimer::pararTiempo()
{
    g_timer_stop(tiempo);
    this->tiempoActivo=false;
}

void EasyTimer::continuarTiempo()
{
    if(!tiempoActivo){
        g_timer_continue(tiempo);
        this->tiempoActivo = true;
    }
}

/*
 * Esta funcion obliga a resetear el cronometro
 * pero no lo detiene
 */
void EasyTimer::resetearTiempo()
{
    g_timer_reset(tiempo);
}

/* stringstream no es lo más eficiente
 * pero no existen diferencias apreciables
 * entre el tiempo real y el del cronometro*/
std::string EasyTimer::getCadenaTiempo()
{
    std::stringstream msg;

    if(this->estado==ADELANTE){
        msg << "0" <<  this->getMinutosHaciaDelante() << ":";
        if(this->getSegundosHaciaDelante()<10){
             msg << "0";
        }
        msg << this->getSegundosHaciaDelante();
     }
     else{
        msg << "0" << this->minuto << ":";
        if(this->getSegundosHaciaAtras()<10){
            msg << "0";
        }
        msg << this->getSegundosHaciaAtras();
    }
    return msg.str();
}

int EasyTimer::getMinutosHaciaDelante()
{
    return (int) (g_timer_elapsed(tiempo,0))/60;

}

int EasyTimer::getSegundosHaciaDelante()
{
    return (int)(g_timer_elapsed(tiempo,0))%60;

}

int EasyTimer::getMinutosHaciaAtras()
{
    return this->minuto;

}

int EasyTimer::getSegundosHaciaAtras()
{
    if(this->minuto==0 && this->segundo -  (int)(g_timer_elapsed(tiempo,0))<=0){
         this->tiempoActivo = false;
    }
    else if(  this->minuto!=0 && this->segundo -  (int)(g_timer_elapsed(tiempo,0))<0){
        this->segundo = 59;
        this->minuto -= 1;
        this->resetearTiempo();
    }


    return this->segundo -  (int)(g_timer_elapsed(tiempo,0));
}
