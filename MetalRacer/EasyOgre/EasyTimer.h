#ifndef EASYTIMER_H
#define EASYTIMER_H

#define ADELANTE 1
#define ATRAS 0

#include <glib.h>
#include <string>
#include <sstream>

class EasyTimer
{
public:
    EasyTimer();
    EasyTimer(int estado,int m,int s);
    ~EasyTimer();

    bool getTiempoActivo();
    int getMinuto();
    int getSegundo();
    void pararTiempo();
    void continuarTiempo();
    void resetearTiempo();
    std::string getCadenaTiempo();

private:
    GTimer* tiempo;
    int estado;
    int minuto;
    int segundo;
    bool tiempoActivo;
    int getMinutosHaciaDelante();
    int getSegundosHaciaDelante();
    int getMinutosHaciaAtras();
    int getSegundosHaciaAtras();
};

#endif // EASYTIMER_H
