#include "EasyCamera.h"

EasyCamera::EasyCamera()
{
}

EasyCamera::~EasyCamera()
{
    cout << "Destructor de la camara" << endl;
}

EasyCamera::EasyCamera(String name, SceneManager *sceneManager)
{
    _name = name;
    _sceneManager = sceneManager;


    _cameraNode = _sceneManager->getRootSceneNode()->createChildSceneNode(_name);
    _targetNode = _sceneManager->getRootSceneNode()->createChildSceneNode(_name + "_target");

    // The camera will always look at the camera target
    _cameraNode->setAutoTracking(true, _targetNode);
    _cameraNode->setFixedYawAxis(true);

    _camera = _sceneManager->createCamera(_name);
    _camera->setNearClipDistance(0.1);
    _camera->setFarClipDistance(100);
    _camera->setPosition(0.0, 0.0, 0.0);
    _cameraNode->attachObject(_camera);

    // Default tightness
    _tightness = 0.03f;
}

void EasyCamera::setTightness(Real tightness)
{
    _tightness = tightness;
}

Real EasyCamera::getTightness()
{
    return _tightness;
}

Camera* EasyCamera::getCamera()
{
    return _camera;
}

Vector3 EasyCamera::getCameraPosition()
{
    return _cameraNode->getPosition();
}

void EasyCamera::instantUpdate(Vector3 cameraPosition, Vector3 targetPosition)
{
    _cameraNode->setPosition(cameraPosition);
    _targetNode->setPosition(targetPosition);
}

void EasyCamera::update(Vector3 cameraPosition, Vector3 targetPosition)
{
    _displacement = (cameraPosition - _cameraNode->getPosition()) * _tightness;
//    _displacement = (cameraPosition - _cameraNode->getPosition());

    _cameraNode->translate(_displacement);

    _displacement = (targetPosition - _targetNode->getPosition()) * _tightness;
//    _displacement = (targetPosition - _targetNode->getPosition());

    _targetNode->translate(_displacement);
}
