﻿#include "EasyOgre.h"

template<> EasyOgre* Ogre::Singleton<EasyOgre>::msSingleton = 0;

EasyOgre::EasyOgre(Ogre::String gameName)
{
    _gameName = gameName;

    _root = new Ogre::Root();

//    Ogre::LogManager* logManager = new Ogre::LogManager();
//    _debugLog = Ogre::LogManager::getSingleton().createLog("debug.log", true, true, false);

    _exitGame=false;
}

EasyOgre::~EasyOgre()
{
//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("Cerrando OGRE...");

    // Closing SDL
    atexit(SDL_Quit);
    atexit(Mix_CloseAudio);

    // Closing OIS
    delete _inputMgr;

    // Closing OGRE
    if(_root){
        delete _root;
    }
}

bool EasyOgre::configure()
{
    if (!_root->restoreConfig()) {
      if (!_root->showConfigDialog()) {
        return false;
      }
    }

    _renderWindow = _root->initialise(true, _gameName);
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    initSDL();
    initCEGUI();
    initMusic();
    return true;
}


bool EasyOgre::loadResources(Ogre::String file)
{
    Ogre::String secName, typeName, archName;
    Ogre::ConfigFile cf;

    cf.load(file);

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }


//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("Recursos Cargados del archivo " + file);

    return true;
}

bool EasyOgre::initSDL()
{
    // Initialization SDL
    if(SDL_Init(SDL_INIT_AUDIO) < 0){
        return false;
    }

    // Initialization SDL mixer
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0){
        return false;
    }

//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("SDL Iniciado...");

    return true;
}

bool EasyOgre::initCEGUI()
{
    _renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("fuenteCtrl_freak");
    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("CEGUI Iniciado...");
}

bool EasyOgre::initMusic()
{
    m_pAudioMgr = new TrackManager();
    m_pSoundMgr = new SoundFXManager();
}


void EasyOgre::addState(GameState *state)
{
    _statesAll.push_back(state);
}

GameState *EasyOgre::findByName(Ogre::String state)
{    std::vector<GameState*>::iterator it;
    GameState* result = 0;

    for(it=_statesAll.begin(); it!=_statesAll.end(); it++){
        if((*it)->getName()==state){
            result = *it;
        }
    }

    return result;
}

void EasyOgre::changeState(Ogre::String state)
{
    if(!_statesActive.empty()){

        _statesActive.back()->exit();
        _statesActive.pop_back();
    }

    _statesActive.push_back(findByName(state));

    _statesActive.back()->enter();

//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("Cambiado a estado " + _statesActive.back()->getName());

}

void EasyOgre::pushState(Ogre::String state)
{
    // Pause current state
    if(!_statesActive.empty()){
        _statesActive.back()->pause();
    }

    // Change to the new state
    _statesActive.push_back(findByName(state));

    // Enter the new state
    _statesActive.back()->enter();
}

void EasyOgre::popState()
{
    // Clean current state
    if (!_statesActive.empty()){
        _statesActive.back()->exit();
        _statesActive.pop_back();
    }

    // Back to previous state
    if (!_statesActive.empty()){
        _statesActive.back()->resume();
    }
}

void EasyOgre::start(Ogre::String state)
{
    _inputMgr = new OISManager;
    _inputMgr->initialise(_renderWindow);

    // KeyListener & MouseListener
    _inputMgr->addKeyListener(this, "EasyOgre");
    _inputMgr->addMouseListener(this, "EasyOgre");

    // FrameListener
    _root->addFrameListener(this);

    // Random
    srand((unsigned)time(0));

    changeState(state);
//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("Start Rendering");
    _root->startRendering();
//    EasyOgre::getSingletonPtr()->_debugLog->logMessage("End Rendering");

}

EasyOgre &EasyOgre::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

EasyOgre *EasyOgre::getSingletonPtr()
{
    return msSingleton;
}



bool EasyOgre::keyPressed(const OIS::KeyEvent &keyEventRef)
{

    if(keyEventRef.key == OIS::KC_ESCAPE){
//        _exitGame = true;
    }

    _statesActive.back()->keyPressed(keyEventRef);

    return true;
}

bool EasyOgre::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    _statesActive.back()->keyReleased(keyEventRef);

    if(keyEventRef.key == OIS::KC_SYSRQ){
        _renderWindow->writeContentsToTimestampedFile(_gameName, ".png");
    }

    return true;
}

bool EasyOgre::mouseMoved(const OIS::MouseEvent &evt)
{
    _statesActive.back()->mouseMoved(evt);

    return true;
}

bool EasyOgre::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    _statesActive.back()->mousePressed(evt, id);
    return true;
}

bool EasyOgre::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    _statesActive.back()->mouseReleased(evt, id);
    return true;
}

bool EasyOgre::frameStarted(const Ogre::FrameEvent &evt)
{
    _inputMgr->capture();
    return _statesActive.back()->frameStarted(evt);

}

bool EasyOgre::frameEnded(const Ogre::FrameEvent &evt)
{
    if(_exitGame){
        return false;
    }

    return _statesActive.back()->frameEnded(evt);

}



