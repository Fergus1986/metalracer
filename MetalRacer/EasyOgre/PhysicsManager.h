#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

// Shape types
#define BOX         0
#define CONVEX      2
#define CYLINDER    3
#define SPHERE      4
#define TRIMESH     5

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>
#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsShape.h>

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

class PhysicsManager
{
public:
    PhysicsManager(Ogre::SceneManager *sceneManager, const Ogre::AxisAlignedBox &bounds, const Ogre::Vector3 &gravity);
    DynamicsWorld *getWorld();

    SceneManager* _sceneManager;
    SceneNode* _debugNode;

    DynamicsWorld* _world;
    DebugDrawer* _debugDrawer;
};

#endif // PHYSICSMANAGER_H
