#ifndef EASYOGRE_H
#define EASYOGRE_H

#include <iostream>

#include <OGRE/Ogre.h>
#include <OIS/OIS.h>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "EasyOgre/GameState.h"
#include "EasyOgre/OISManager.h"

#include "Sound/TrackManager.hpp"
#include "Sound/SoundFXManager.hpp"

using namespace std;

class GameState;

class EasyOgre:
        public Ogre::Singleton<EasyOgre>,
        public Ogre::FrameListener,
        public OIS::KeyListener,
        public OIS::MouseListener
{
public:
    EasyOgre(Ogre::String gameName);
    ~EasyOgre();

    bool configure();
    bool loadResources(Ogre::String file);
    bool initSDL();
    bool initCEGUI();
    bool initMusic();

    // States
    void addState(GameState* state);
    GameState* findByName(Ogre::String state);

    void changeState(Ogre::String state);
    void pushState(Ogre::String state);
    void popState();

    void start(Ogre::String state);

    // Singleton
    static EasyOgre& getSingleton ();
    static EasyOgre* getSingletonPtr();

    // KeyListener
    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    // MouseListener
    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    // FrameListener.
    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

    // CEGUI
    CEGUI::OgreRenderer* _renderer;

    Ogre::String _gameName;
    Ogre::Root* _root;
    Ogre::RenderWindow* _renderWindow;
    Ogre::SceneManager* _sceneManager;
    Ogre::Log* _debugLog;

    // Gestor de eventos de entrada.
    OISManager* _inputMgr;

    std::vector<GameState*> _statesAll;
    std::vector<GameState*> _statesActive;

    bool _exitGame;

    // MUSIC
    TrackManager*				m_pAudioMgr;
    SoundFXManager*				m_pSoundMgr;




};

#endif // EASYOGRE_H
