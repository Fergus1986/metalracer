#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <OGRE/Ogre.h>
#include <OIS/OIS.h>

#include "EasyOgre/OISManager.h"


class GameState
{
public:
    GameState(Ogre::String name){_name = name;}

    virtual Ogre::String getName() = 0;

    virtual void enter() = 0;
    virtual void exit() = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;

    virtual void keyPressed (const OIS::KeyEvent &e) = 0;
    virtual void keyReleased (const OIS::KeyEvent &e) = 0;

    virtual void mouseMoved (const OIS::MouseEvent &e) = 0;
    virtual void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) = 0;
    virtual void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) = 0;

    virtual bool frameStarted (const Ogre::FrameEvent& evt) = 0;
    virtual bool frameEnded (const Ogre::FrameEvent& evt) = 0;

protected:
    Ogre::String _name;


};

#endif // GAMESTATE_H
