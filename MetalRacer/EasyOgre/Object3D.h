#ifndef OBJECT3D_H
#define OBJECT3D_H

#include "EasyOgre/PhysicsManager.h"

#include <OGRE/Ogre.h>
#include <OGRE/OgreProgressiveMesh.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include <sstream>


using namespace Ogre;
using namespace std;


class Object3D
{
public:
    Object3D();

    // Graphical object without Physical object
    Object3D(const String name,
             const Vector3 position,
             const Quaternion rotation,
             SceneNode *nodeParent,
             SceneManager *sceneManager);

    // Graphical object with full Physical object
    Object3D(const String name,
             const Vector3 position,
             const Quaternion rotation,
             const float restitution,
             const float friction,
             const float mass,
             Ogre::SceneNode* nodeParent,
             const int shapeType,
             Ogre::SceneManager* sceneManager,
             PhysicsManager* physicsManager);

    // Special Constructor for Squares
    Object3D(const String name,
             const Vector3 position,
             const Quaternion rotation,
             const float restitution,
             const float friction,
             const float mass,
             Ogre::SceneNode* nodeParent,
             Ogre::Entity* entity,
             OgreBulletCollisions::CollisionShape* shape,
             Ogre::SceneManager* sceneManager,
             PhysicsManager* physicsManager);


    ~Object3D();

protected:
    // Graphical object
    Ogre::SceneManager* _sceneManager;
    Ogre::String _name;
    Ogre::SceneNode* _nodeParent;
    Ogre::SceneNode* _node;
    Ogre::Entity* _entity;

    // Physical object
    PhysicsManager* _physicsManager;
    OgreBulletDynamics::RigidBody* _body;
    OgreBulletCollisions::StaticMeshToShapeConverter *_converter;
    OgreBulletCollisions::CollisionShape* _shape;
    float _restitution;
    float _friction;
    float _mass;
    int _shapeType;

    // Position & Rotation
    Ogre::Vector3 _position;
    Ogre::Quaternion _rotation;

private:

    void createGraphicalObject();
    void setGraphicalObject();
    void createBody();
    void createPhysicalObject();
    void setPhysicalObject();


};

#endif // OBJECT3D_H
