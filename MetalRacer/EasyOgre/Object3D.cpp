#include "Object3D.h"

Object3D::Object3D()
{
}



// Graphical object without Physical object
Object3D::Object3D(const String name,
                   const Vector3 position,
                   const Quaternion rotation,
                   SceneNode *nodeParent,
                   SceneManager *sceneManager)
{
    _name = name;
    _position = position;
    _rotation = rotation;
    _nodeParent = nodeParent;
    _sceneManager = sceneManager;

    _physicsManager = NULL;
    _restitution = 0.0f;
    _friction = 0.0f;
    _mass = 0.0f;
    _body = NULL;
    _shape = NULL;
    _shapeType = -1;

    createGraphicalObject();
    setGraphicalObject();
}

// Graphical object with full Physical object
Object3D::Object3D(const String name,
                   const Vector3 position,
                   const Quaternion rotation,
                   const float restitution,
                   const float friction,
                   const float mass,
                   SceneNode *nodeParent,
                   const int shapeType,
                   SceneManager *sceneManager,
                   PhysicsManager *physicsManager)
{

    _sceneManager = sceneManager;
    _physicsManager = physicsManager;
    _name = name;
    _position = position;
    _rotation = rotation;
    _restitution = restitution;
    _friction = friction;
    _mass = mass;
    _nodeParent = nodeParent;
    _shapeType = shapeType;

    createGraphicalObject();
    setGraphicalObject();
    createBody();
    createPhysicalObject();
    setPhysicalObject();
}
// Special Constructor for Squares
Object3D::Object3D(const String name,
                   const Vector3 position,
                   const Quaternion rotation,
                   const float restitution,
                   const float friction,
                   const float mass,
                   SceneNode *nodeParent,
                   Entity *entity,
                   CollisionShape *shape,
                   SceneManager *sceneManager,
                   PhysicsManager *physicsManager)
{

    _sceneManager = sceneManager;
    _physicsManager = physicsManager;
    _name = name;
    _position = position;
    _rotation = rotation;
    _restitution = restitution;
    _friction = friction;
    _mass = mass;
    _nodeParent = nodeParent;
    _shapeType = -1;
    _shape = shape;

    createGraphicalObject();
    setGraphicalObject();
    createBody();
    setPhysicalObject();

}

Object3D::~Object3D()
{
}

void Object3D::createGraphicalObject()
{
    _entity = _sceneManager->createEntity(_name.append(".mesh"));
//    Ogre::MeshPtr mesh = _entity->getMesh();
//    Ogre::Mesh::LodValueList lod;

//    lod.push_back(40);
//    lod.push_back(80);
//    lod.push_back(120);
//    lod.push_back(160);

//    mesh->generateLodLevels(lod, ProgressiveMesh::VertexReductionQuota::VRQ_PROPORTIONAL, 0.1);


//    cout << "Graphical Object: " <<  _name << " created" << endl;
}


void Object3D::setGraphicalObject()
{
    _node = _sceneManager->createSceneNode();
    _node->attachObject(_entity);
    _nodeParent->addChild(_node);
//    cout << "nodeParent: " << _nodeParent->getName() << endl;
//    cout << "node: " << _node->getName() << endl;
}

void Object3D::createBody()
{
    _body = new RigidBody(_node->getName(), _physicsManager->getWorld());
}

void Object3D::createPhysicalObject()
{
    _shape = NULL;
    _converter = new OgreBulletCollisions:: StaticMeshToShapeConverter(_entity);

    switch(_shapeType){

    case BOX:
        _shape =_converter->createBox();
        break;

    case CONVEX:
        _shape =_converter->createConvex();
        break;

    case CYLINDER:
//        _shape =_converter->createCylinder();
        break;

    case SPHERE:
        _shape = _converter->createSphere();
        break;

    case TRIMESH:
        _shape = _converter->createTrimesh();
        break;

    default:
        _shape =_converter->createBox();
        break;
    }

//    cout << "Physical Object: " <<  _node->getName() << " created" << endl;

}

void Object3D::setPhysicalObject()
{
    _body->setShape(_node, _shape, _restitution, _friction, _mass, _position, _rotation);
//    cout << "Physical Object: " <<  _node->getName() << " added" << endl;

}
