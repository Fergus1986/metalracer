#include "IntroState.h"

IntroState::IntroState(String name): GameState(name)
{
    _exitGame = false;
}

IntroState::~IntroState()
{
}

String IntroState::getName()
{
    return this->_name;
}

void IntroState::enter()
{
    cout << "Intro" << endl;
        _root = Ogre::Root::getSingletonPtr();
        _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "IntroSceneManager");

        _camera = new EasyCamera("IntroCamera", _sceneManager);

        _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
        // Nuevo background colour.
        _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

        Real width = _viewport->getActualWidth();
        Real height = _viewport->getActualHeight();
        _camera->getCamera()->setAspectRatio(width/height);

        // Overlays
        _overlayManager = Ogre::OverlayManager::getSingletonPtr();

        createScene();
}

void IntroState::exit()
{
    if(_sceneManager){
            _sceneManager->destroyAllCameras();
            _sceneManager->clearScene();
            _root->destroySceneManager(_sceneManager);
            _root->getAutoCreatedWindow()->removeAllViewports();
    //        delete _sceneManager;
        }
        _overlayIntro->hide();

        cout << "IntroState exiting..." << endl;
}

void IntroState::pause()
{
}

void IntroState::resume()
{
}

void IntroState::keyPressed(const OIS::KeyEvent &e)
{
}

void IntroState::keyReleased(const OIS::KeyEvent &e)
{
    switch(e.key)
    {

    case OIS::KC_ESCAPE:
        _exitGame = true;
        break;
    case OIS::KC_RETURN:
        EasyOgre::getSingleton().changeState("MenuState");
    }
}

void IntroState::mouseMoved(const OIS::MouseEvent &e)
{
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void IntroState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

bool IntroState::frameStarted(const FrameEvent &evt)
{
    return true;
}

bool IntroState::frameEnded(const FrameEvent &evt)
{
    if(_exitGame)
        return false;
    return true;
}

void IntroState::createScene()
{
    _overlayIntro= _overlayManager->getByName("Intro");
        _overlayIntro->show();
}



