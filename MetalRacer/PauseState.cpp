#include "PauseState.h"

PauseState::PauseState(String name): GameState(name)
{
}

PauseState::~PauseState()
{
}

String PauseState::getName()
{
    return _name;
}

void PauseState::enter()
{
    cout << "Enter Pause" << endl;
    //    _root = Ogre::Root::getSingletonPtr();
    //    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "PausaSceneManager");

    //    _camera = new EasyCamera("PauseCamera", _sceneManager);

    //    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
    // Nuevo background colour.
    //    _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

    //    Real width = _viewport->getActualWidth();
    //    Real height = _viewport->getActualHeight();
    //    _camera->getCamera()->setAspectRatio(width/height);

    // CEGUI
    _ceguiSystem = CEGUI::System::getSingletonPtr();

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    createScene();
}

void PauseState::exit()
{
    //    if(_sceneManager){
    //        cout << "prueba" << endl;
    //        _sceneManager->destroyAllCameras();
    //        _sceneManager->clearScene();
    //        _root->destroySceneManager(_sceneManager);
    //        _root->getAutoCreatedWindow()->removeAllViewports();
    ////        delete _sceneManager;
    //    }

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();

    _overlayPausa->hide();

    cout << "PauseState exiting..." << endl;

}

void PauseState::pause()
{
}

void PauseState::resume()
{
}

void PauseState::createScene()
{
    cout << "Crear Escena" << endl;

    _overlayPausa = _overlayManager->getByName("Pausa");
    cout << "Overlay: " <<  _overlayPausa->getName() << endl;
    _overlayPausa->show();

    _ceguiWindow = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","pauseWindow");
    CEGUI::SchemeManager::getSingleton().create("Menu.scheme");
    this->crearPausa();
    _ceguiWindow->setVisible(true);
    CEGUI::System::getSingleton().setGUISheet(_ceguiWindow);
    CEGUI::MouseCursor::getSingleton().show();
}

void PauseState::crearPausa()
{
    //este boton solo se muestra, no se hace nada en el
    // pauseButton =CEGUI::WindowManager::getSingleton().createWindow("Pausa/BotonPausa", "pauseMenu/PauseButton");
    // pauseButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.3417,0),CEGUI::UDim(0.1302,0)));
    // pauseButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.3320,0),CEGUI::UDim(0.2148,0)));
    //Boton para volver al juego
    backButton = CEGUI::WindowManager::getSingleton().createWindow("Pausa/BotonVolver", "pauseMenu/BackButton");
    backButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.20,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.16,0),CEGUI::UDim(0.4,0)));
    //Boton para volver al Menu Principal
    menuButton = CEGUI::WindowManager::getSingleton().createWindow("Pausa/BotonMenu", "pauseMenu/MenuButton");
    menuButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.20,0),CEGUI::UDim(0.05,0)));
    menuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.16,0),CEGUI::UDim(0.5,0)));

    backButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&PauseState::continuar, this));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&PauseState::salir, this));



    //_ceguiWindow->addChildWindow(pauseButton);
    _ceguiWindow->addChildWindow(backButton);
    _ceguiWindow->addChildWindow(menuButton);

}

void PauseState::keyPressed(const OIS::KeyEvent &e)
{

}

void PauseState::keyReleased(const OIS::KeyEvent &e)
{
    if(e.key == OIS::KC_ESCAPE || e.key== OIS::KC_P)
    {
        EasyOgre::getSingleton().popState();
    }
}

void PauseState::mouseMoved(const OIS::MouseEvent &e)
{
    _ceguiSystem->injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel
    if (e.state.Z.rel) _ceguiSystem->injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void PauseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));

}

void PauseState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));

}

bool PauseState::frameStarted(const FrameEvent &evt)
{
    return true;
}

bool PauseState::frameEnded(const FrameEvent &evt)
{
    return true;
}

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

bool PauseState::continuar(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().popState();
    return true;
}

bool PauseState::salir(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().popState();
    EasyOgre::getSingleton().changeState("MenuState");
    return true;
}
