#ifndef SQUARETYPE_H
#define SQUARETYPE_H


#include "EasyOgre/PhysicsManager.h"

#include <OgreBullet/Collisions/OgreBulletCollisions.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsShape.h>

#include <OGRE/Ogre.h>

using namespace Ogre;

class SquareType
{
public:
    SquareType();
    SquareType(const int id,
               const String name,
               const int rotation,
               const float restitution,
               const float friction,
               const float mass,
               SceneManager* sceneManager);

    ~SquareType();

    int getID();
    Ogre::String getName();
    float getRestitution();
    float getFriction();
    float getMass();
    int getRotation();

    Ogre::Entity* getEntity();
    OgreBulletCollisions::CollisionShape* getShape();


private:

    SceneManager* _sceneManager;
    int _id;
    Ogre::String _name;
    float _restitution;
    float _friction;
    float _mass;
    int _rotation;

    // Graphical object
    Ogre::Entity* _entity;

    // Physical object
    OgreBulletCollisions::StaticMeshToShapeConverter* _converter;
    OgreBulletCollisions::CollisionShape* _shape;



};

#endif // SQUARETYPE_H
