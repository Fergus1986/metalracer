#include "CircuitManager.h"

CircuitManager::CircuitManager(SceneManager *sceneManager, PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

}

CircuitManager::~CircuitManager()
{
}

Circuit *CircuitManager::loadCircuit(const Ogre::String name)
{
    loadFile(name);

    _circuit = new Circuit (name, _sceneManager, _physicsManager);

    cout << "SquareTypes: " << _squareTypes.size() << endl;
    cout << "Squares: " << _squaresID.size() << endl;


    int squareIndex=-1;
    int squareType=-1;
    Ogre::String squareName="";

    _circuit->setDimensions(_layerWidth, _layerHeight);

    // Circuit
    for(int i=0; i<_layerWidth; i++){
        for(int z = 0; z<_layerHeight; z++){
            squareIndex = (z * _layerWidth) + i;
            squareType = _squaresID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{
                squareName = _squareTypes.at(squareType)->getName();
                _circuit->addSquare(squareName, Ogre::Vector3(20*i,0, 20*z), _squareTypes.at(squareType));
            }
        }
    }

    // Track

    cout << "Calculate Track..." << endl;

    for(int x=0; x<_layerWidth; x++){
        for(int z = 0; z<_layerHeight; z++){
            stringstream ss;
            ss << x;
            ss << "_";
            ss << z;
            squareIndex = (z * _layerWidth) + x;
            squareType = _trackID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
                _circuit->addTrackPoint("empty", x, z);
            }else{
                squareName = _squareTypes.at(squareType)->getName();

                _circuit->addTrackPoint(squareName + "_" + ss.str(), x, z);

                if(squareName.compare("checkpoint")==0){
                    _circuit->addCheckPoint(squareName + "_" + ss.str(), x, z);
                }

                if(squareName.compare("start")==0){
                    _circuit->setStart(squareName + "_" + ss.str(), x, z);
                }
            }
        }
    }

    _circuit->generateTrack(_circuit->getStart(),0);

    return _circuit;

}

void CircuitManager::loadFile(const Ogre::String name)
{
    cout << "Reading file... " << name << endl;
    // Initialiation
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cerr << "Error in initialization! :\n"
             << message << "\n";
        XMLString::release(&message);
        return;
    }

    XercesDOMParser* parser = new XercesDOMParser();
    parser->setValidationScheme(XercesDOMParser::Val_Always);

    // Parsing the xml file
    try {
        parser->parse(name.c_str());
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cout << "Excepción capturada: \n"
             << message << "\n";
        XMLString::release(&message);
    }
    catch (const DOMException& toCatch) {
        char* message = XMLString::transcode(toCatch.msg);
        cout << "Excepción capturada: \n"
             << message << "\n";
        XMLString::release(&message);
    }
    catch (...) {
        cout << "Excepción no esperada.\n" ;
        return;
    }

    DOMDocument* xmlDoc;
    DOMElement* elementRoot;

    try {
        // Obtener el elemento raíz del documento.
        xmlDoc = parser->getDocument();
        elementRoot = xmlDoc->getDocumentElement();

        if(!elementRoot)
            throw(std::runtime_error("Documento XML vacío."));

    }
    catch (xercesc::XMLException& e ) {
        char* message = xercesc::XMLString::transcode( e.getMessage() );
        ostringstream errBuf;
        errBuf << "Error 'parseando': " << message << flush;
        XMLString::release( &message );
        return;
    }

    XMLCh* square = XMLString::transcode("tileset");
    XMLCh* layer = XMLString::transcode("layer");

    // Procesando los nodos hijos del raíz...
    for (XMLSize_t i = 0; i < elementRoot->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = elementRoot->getChildNodes()->item(i);

        if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Node <tileset>?
            if (XMLString::equals(node->getNodeName(), square)){
                // Parser square
                parserSquare(node);
            }else{
                // Node <layer>?
                if (XMLString::equals(node->getNodeName(), layer)){
                    // Parser layer
                    parserLayer(node);
                }
            }
        }
    }

    // Liberar recursos.
    XMLString::release(&square);
    XMLString::release(&layer);

    delete parser;
}

void CircuitManager::parserSquare(DOMNode *square)
{
    // Square Attributes
    int squareID;
    Ogre::String squareName;
    float squareFriction;
    float squareMass;
    float squareRestitution;
    int squareRotation;

    DOMNamedNodeMap* attributesSquare = square->getAttributes();
    DOMNode* id = attributesSquare->getNamedItem(XMLString::transcode("firstgid"));
    DOMNode* name = attributesSquare->getNamedItem(XMLString::transcode("name"));

    squareID = atoi(XMLString::transcode(id->getNodeValue()));
    squareName = XMLString::transcode(name->getNodeValue());

    XMLCh* properties = XMLString::transcode("properties");
    XMLCh* friction = XMLString::transcode("friction");
    XMLCh* mass = XMLString::transcode("mass");
    XMLCh* restitution = XMLString::transcode("restitution");
    XMLCh* rotation = XMLString::transcode("rotation");
    
    // Property Attributes
    for (XMLSize_t i = 0; i < square->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = square->getChildNodes()->item(i);

        // Node <properties>?
        if (node->getNodeType() == DOMNode::ELEMENT_NODE){
            if (XMLString::equals(node->getNodeName(), properties)){
                for (XMLSize_t j = 0; j < node->getChildNodes()->getLength(); ++j) {
                    DOMNode* nodeProperty = node->getChildNodes()->item(j);
                    // Node <property>?
                    if (nodeProperty->getNodeType() == DOMNode::ELEMENT_NODE){
                        DOMNamedNodeMap* attributesProperty = nodeProperty->getAttributes();

                        DOMNode* nameProperty = attributesProperty->getNamedItem(XMLString::transcode("name"));
                        DOMNode* valueProperty = attributesProperty->getNamedItem(XMLString::transcode("value"));

                        // Friction
                        if (XMLString::equals(nameProperty->getNodeValue(), friction)){
                            squareFriction = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                        // Mass
                        else if (XMLString::equals(nameProperty->getNodeValue(), mass)){
                            squareMass = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                        // Restitution
                        else if (XMLString::equals(nameProperty->getNodeValue(), restitution)){
                            squareRestitution = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                        // Rotation
                        else if (XMLString::equals(nameProperty->getNodeValue(), rotation)){
                            squareRotation = atoi(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                    }
                }
            }
        }
    }

    // SquareType
    SquareType* squareType = new SquareType(squareID, squareName, squareRotation, squareRestitution, squareFriction, squareMass, _sceneManager);
    _squareTypes.push_back(squareType);


    //    cout << "id: " << squareID << " name: " << squareName << endl;
    //    cout << "friction: " << squareFriction << " mass: " << squareMass << " restitution: " << squareRestitution << endl;
}



void CircuitManager::parserLayer(DOMNode *layer)
{
    // Layer Attributes
    DOMNamedNodeMap* attributes = layer->getAttributes();

    DOMNode* name = attributes->getNamedItem(XMLString::transcode("name"));
    DOMNode* width = attributes->getNamedItem(XMLString::transcode("width"));
    DOMNode* height = attributes->getNamedItem(XMLString::transcode("height"));


    Ogre::String layerName = (XMLString::transcode(name->getNodeValue()));
    _layerWidth = atoi(XMLString::transcode(width->getNodeValue()));
    _layerHeight = atoi(XMLString::transcode(height->getNodeValue()));

    XMLCh* data_ch = XMLString::transcode("data");
    XMLCh* title_ch = XMLString::transcode("tile");

    for (XMLSize_t i = 0; i < layer->getChildNodes()->getLength(); ++i ) {

        DOMNode* nodeData = layer->getChildNodes()->item(i);

        if (nodeData->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Nodo <data>?
            if (XMLString::equals(nodeData->getNodeName(), data_ch)){

                for (XMLSize_t j = 0; j < nodeData->getChildNodes()->getLength(); ++j ) {

                    DOMNode* nodeTile = nodeData->getChildNodes()->item(j);

                    if (nodeTile->getNodeType() == DOMNode::ELEMENT_NODE) {
                        // Nodo <tile>?
                        if (XMLString::equals(nodeTile->getNodeName(), title_ch)){

                            // Tile attributes
                            DOMNamedNodeMap* TileAttributes = nodeTile->getAttributes();
                            DOMNode* tile = TileAttributes->getNamedItem(XMLString::transcode("gid"));
                            int square = atoi(XMLString::transcode(tile->getNodeValue()));

                            if(layerName.compare("circuit")==0){
                                _squaresID.push_back(square);
//                                cout << "Circuit: " << square << endl;

                            }else{
                                _trackID.push_back(square);
//                                cout << "Track: " << square << endl;
                            }

                        }
                    }
                }
            }
        }
    }
}


