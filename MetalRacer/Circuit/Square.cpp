#include "Square.h"

Square::Square()
{
}

Square::Square(const String name,
               const Ogre::Vector3 position,
               SquareType *type,
               Ogre::SceneNode* nodeParent,
               Ogre::SceneManager* sceneManager,
               PhysicsManager* physicsManager)
{
    _position = position;
    _type = type;
    _nodeParent = nodeParent;

    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _rotation = Quaternion(Radian(Degree(type->getRotation())),Ogre::Vector3::UNIT_Y);

     _objecto3d = new Object3D(name,
                               _position,
                               _rotation,
                               type->getRestitution(),
                               type->getFriction(),
                               type->getMass(),
                               _nodeParent,
                               type->getEntity(),
                               type->getShape(),
                               _sceneManager,
                               _physicsManager);

}

Square::~Square()
{
}
