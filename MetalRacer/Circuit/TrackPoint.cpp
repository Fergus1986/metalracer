#include "TrackPoint.h"

TrackPoint::TrackPoint()
{
}

TrackPoint::TrackPoint(Ogre::String name, const int x, const int z)
{
    _name = name;
    _x = x;
    _z = z;
    _studied=false;
    _parent = NULL;
}

TrackPoint::~TrackPoint()
{
}

Ogre::String TrackPoint::getName()
{
    return _name;
}

int TrackPoint::getX()
{
    return _x;
}

int TrackPoint::getZ()
{
    return _z;
}

bool TrackPoint::getStudied()
{
    return _studied;
}

void TrackPoint::setStudied(bool studied)
{
    _studied = studied;
}

void TrackPoint::setParent(TrackPoint *parent)
{
    _parent = parent;
}

TrackPoint *TrackPoint::getParent()
{
    return _parent;
}

