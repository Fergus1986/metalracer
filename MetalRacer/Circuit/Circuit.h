#ifndef CIRCUIT_H
#define CIRCUIT_H

#include <vector>
#include <map>

#include "EasyOgre/PhysicsManager.h"
#include "Circuit/Square.h"
#include "Circuit/SquareType.h"
#include "Circuit/TrackPoint.h"

#include "Util/DynamicArray.h"

#include <OGRE/Ogre.h>


using namespace Ogre;
using namespace std;

class Circuit
{
public:
    Circuit();
    Circuit(const Ogre::String name,
            Ogre::SceneManager* sceneManager,
            PhysicsManager* physicsManager);

    Circuit(const Ogre::String name,
            const int width,
            const int height,
            const Ogre::String audio,
            Ogre::SceneManager* sceneManager,
            PhysicsManager* physicsManager);

    ~Circuit();

    void setDimensions(const int width, const int height);
    void setAudio(const Ogre::String audio);
    void addSquare(const Ogre::String name,
                   const Ogre::Vector3 position,
                   SquareType *type);

    void addTrackPoint(Ogre::String name, const int x, const int z);
    void addCheckPoint(Ogre::String name, const int x, const int z);
    void setStart(Ogre::String name, const int x, const int z);
    TrackPoint* getStart();

    std::vector<TrackPoint*> getNeighbors(TrackPoint* track);

    void generateTrack(TrackPoint* origin, int checkpoint);
    std::list<TrackPoint *> getTrack();




//private:

    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    Ogre::String _name;
    int _width;
    int _height;
    Ogre::String _audio;

    Ogre::SceneNode* _nodeCircuit;
    std::vector<Square*> _circuit;

    // Track
    DynamicArray <TrackPoint*> _track;
    std::vector<TrackPoint*> _checkpoints;
    TrackPoint* _start;
    std::list<TrackPoint *>  _trackList;



};

#endif // CIRCUIT_H
