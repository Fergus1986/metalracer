#ifndef TRACKPOINT_H
#define TRACKPOINT_H

#include <OGRE/Ogre.h>

class TrackPoint
{
public:
    TrackPoint();
    TrackPoint(Ogre::String name, const int x, const int z);
    ~TrackPoint();

    Ogre::String getName();
    int getX();
    int getZ();
    bool getStudied();
    void setStudied(bool studied);
    void setParent(TrackPoint* parent);
    TrackPoint* getParent();

private:
    Ogre::String _name;
    int _x;
    int _z;
    bool _studied;
    TrackPoint* _parent;

};

#endif // TRACKPOINT_H
