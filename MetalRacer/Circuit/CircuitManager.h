#ifndef CIRCUITMANAGER_H
#define CIRCUITMANAGER_H

#include "Circuit/Circuit.h"
#include "Circuit/SquareType.h"

#include "EasyOgre/PhysicsManager.h"


#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include <OGRE/Ogre.h>
#include <sstream>
#include <cstdlib>

using namespace Ogre;
using namespace xercesc;

class CircuitManager
{
public:
    CircuitManager(Ogre::SceneManager* sceneManager,
                   PhysicsManager *physicsManager);
    ~CircuitManager();

    Circuit * loadCircuit(const Ogre::String name);


private:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    // Circuit
    Circuit * _circuit;
    int _layerWidth;
    int _layerHeight;
    std::vector<int> _squaresID;

    // Track
    std::vector<int> _trackID;
    std::vector<int> _trackID2;

    // Types of Squares
    std::vector<SquareType*> _squareTypes;
    
    void loadFile(const Ogre::String name);
    void parserSquare(xercesc::DOMNode* square);
    void parserLayer(xercesc::DOMNode* layer);



};

#endif // CIRCUITMANAGER_H
