#include "SquareType.h"

SquareType::SquareType()
{
}

SquareType::SquareType(const int id,
                       const Ogre::String name,
                       const int rotation,
                       const float restitution,
                       const float friction,
                       const float mass,
                       SceneManager *sceneManager)
{
    _sceneManager = sceneManager;
    this->_id = id;
    this->_name = name;
    this->_restitution = restitution;
    this->_friction = friction;
    this->_mass= mass;
    this->_rotation = rotation;

    if( (name.compare("track")) == 0 ||
        (name.compare("start") == 0) ||
        (name.compare("checkpoint")== 0)){
        // No graphical nor physcal object

    }else{
        // Graphical object
        _entity = _sceneManager->createEntity(_name + ".mesh");

        // Physical object
        _converter = new StaticMeshToShapeConverter(_entity);
        _shape = _converter->createTrimesh();
    }
}

SquareType::~SquareType()
{
}

int SquareType::getID()
{
    return this->_id;
}

String SquareType::getName()
{
    return this->_name;
}

float SquareType::getRestitution()
{
    return this->_restitution;
}

float SquareType::getFriction()
{
    return this->_friction;
}

float SquareType::getMass()
{
    return this->_mass;
}

int SquareType::getRotation()
{
    return this->_rotation;
}

Entity *SquareType::getEntity()
{
    return this->_entity;
}

CollisionShape *SquareType::getShape()
{
    return this->_shape;
}
