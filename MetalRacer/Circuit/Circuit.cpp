#include "Circuit.h"

Circuit::Circuit()
{
}

Circuit::Circuit(const String name,
                 SceneManager *sceneManager,
                 PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _name = name;
    _width = -1;
    _height = -1;
    _audio = "";

    // SceneNode
    _nodeCircuit = _sceneManager->createSceneNode(_name);
    _sceneManager->getRootSceneNode()->addChild(_nodeCircuit);
}

Circuit::Circuit(const String name,
                 const int width,
                 const int height,
                 const String audio,
                 SceneManager *sceneManager,
                 PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _name = name;
    _width = width;
    _height = height;
    _audio = audio;

    // SceneNode
    _nodeCircuit = _sceneManager->createSceneNode(_name);
    _sceneManager->getRootSceneNode()->addChild(_nodeCircuit);

//    // Reserve Memory
//    _circuit.reserve(_width*_height);

}

Circuit::~Circuit()
{
}

void Circuit::setDimensions(const int width, const int height)
{
    _width = width;
    _height = height;

    // Reserve Memory
    _circuit.reserve(_width*_height);
    // Track
    _track = DynamicArray<TrackPoint*>(_width,_height);
}

void Circuit::setAudio(const String audio)
{
    _audio = audio;
}

void Circuit::addSquare(const String name,
                        const Vector3 position,
                        SquareType* type)
{


    Square* square = new Square(name,
                                position,
                                type,
                                _nodeCircuit,
                                _sceneManager,
                                _physicsManager);

    _circuit.push_back(square);
}

void Circuit::addTrackPoint(String name, const int x, const int z)
{
//    cout << "addTrackPoint: " << x << "," << z << endl;
    _track[x][z] = new TrackPoint(name, x, z);
}

void Circuit::addCheckPoint(String name, const int x, const int z)
{
//    cout << "addCheckPoint: " << x << "," << z << endl;
    TrackPoint* checkPoint = new TrackPoint(name, x, z);
    _checkpoints.push_back(checkPoint);
}

void Circuit::setStart(String name, const int x, const int z)
{
//    cout << "addStartPoint: " << x << "," << z << endl;
    _start = new TrackPoint(name, x, z);
}

TrackPoint *Circuit::getStart()
{
    return _start;
}

std::vector<TrackPoint *> Circuit::getNeighbors(TrackPoint *track)
{
    std::vector<TrackPoint*> neighbors = std::vector<TrackPoint*>();

    int x = track->getX();
    int z = track->getZ();

//    cout << "p(" << x << "," << z << ")" << "= " << (_track[x][z])->getName() << endl;

    // Up
    x = track->getX();
    z = track->getZ() - 1;
//    cout << "Up(" << x << "," << z << ")" << "= " << (_track[x][z])->getName() << endl;

    if(x >= 0 && x<_width && z>= 0 && z<_height){
        neighbors.push_back((_track[x][z]));
    }

    // Down
    x = track->getX();
    z = track->getZ() + 1;
//    cout << "Down(" << x << "," << z << ")" << "= " << (_track[x][z])->getName() << endl;

    if(x >= 0 && x<_width && z>= 0 && z<_height){
        neighbors.push_back((_track[x][z]));
    }

    // Left
    x = track->getX() -1;
    z = track->getZ();
//    cout << "Left(" << x << "," << z << ")" << "= " << (_track[x][z])->getName() << endl;
    if(x >= 0 && x<_width && z>= 0 && z<_height){
        neighbors.push_back((_track[x][z]));
    }


    // Right
    x = track->getX() + 1;
    z = track->getZ();
//    cout << "Right(" << x << "," << z << ")" << "= " << (_track[x][z])->getName() << endl;
    if(x >= 0 && x<_width && z>= 0 && z<_height){
        neighbors.push_back((_track[x][z]));
    }

    return neighbors;
}

void Circuit::generateTrack(TrackPoint *origin, int checkpoint)
{
    std::vector<TrackPoint*> temporal;
    std::list<TrackPoint*> trackNeighbors;
    TrackPoint* trackStudy;

    int j=0;

    bool seguir = true;
    trackNeighbors.push_back(origin);

    do{
        j++;

        trackStudy = trackNeighbors.front();
//        cout << trackStudy->getName() << endl;

        if(trackStudy->getName().compare(_checkpoints.at(checkpoint)->getName())!=0){

            trackStudy->setStudied(true);
            temporal = getNeighbors(trackStudy);

            for(int i=0; i<temporal.size(); i++){
                if( (temporal.at(i)->getName().compare("empty")!=0)
                        && (temporal.at(i)->getStudied()==false)){
                    temporal.at(i)->setParent(trackStudy);
                    trackNeighbors.push_back(temporal.at(i));
                }
            }
            trackNeighbors.pop_front();
        }else{
            seguir = false;
//            cout << trackStudy->getName() << endl;
        }

        if(j>500){
            seguir = false;
        }

    }while(seguir);


    do{
        _trackList.push_front(trackStudy);
        trackStudy = trackStudy->getParent();

    }while(trackStudy!=NULL);
}


std::list<TrackPoint *> Circuit::getTrack()
{
    return _trackList;
}
