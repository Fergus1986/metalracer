#ifndef SQUARE_H
#define SQUARE_H

#include "EasyOgre/PhysicsManager.h"
#include "EasyOgre/Object3D.h"
#include "Circuit/SquareType.h"

#include <OGRE/Ogre.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>


using namespace Ogre;

class Square
{
public:
    Square();

    Square(const Ogre::String name,
           const Ogre::Vector3 position,
           SquareType* type,
           Ogre::SceneNode* nodeParent,
           Ogre::SceneManager* sceneManager,
           PhysicsManager* physicsManager);

    ~Square();


private:
    PhysicsManager* _physicsManager;
    SceneManager* _sceneManager;

    Object3D * _objecto3d;
    SquareType* _type;
    OgreBulletDynamics::RigidBody* _body;
    Ogre::Vector3 _position;
    Quaternion _rotation;

    Ogre::SceneNode* _nodeSquare;
    Ogre::SceneNode* _nodeParent;
    Ogre::Entity* _entity;

};

#endif // SQUARE_H
