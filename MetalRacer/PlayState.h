#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include <cstdlib>
#include <ctime>
#include <iostream>

#include "EasyOgre/EasyOgre.h"
#include "EasyOgre/GameState.h"
#include "EasyOgre/PhysicsManager.h"
#include "EasyOgre/Object3D.h"
#include "EasyOgre/EasyCamera.h"
#include "EasyOgre/EasyTimer.h"
#include "Circuit/Circuit.h"
#include "Circuit/CircuitManager.h"
#include "Vehicle/Vehicle.h"
#include "Vehicle/VehicleManager.h"
#include "Vehicle/Vehicle.h"
#include "Character/Character.h"
#include "Enemy/Enemy.h"

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>

#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>

using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;
using namespace Ogre;

using namespace std;


class PlayState: public GameState
{
public:
    PlayState(Ogre::String name);
    ~PlayState();

    Ogre::String getName();

    void enter();
    void exit();
    void pause();
    void resume();

    void createScene();
    void createPhysics();
    void createWorld();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

    void drawGUI();
    void hideGUI();

private:

    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physcisManager;
    Ogre::Viewport* _viewport;

    bool _exitGame;

    Character* _character;
    Enemy* _enemy1;
    Enemy* _enemy2;
    Enemy* _enemy3;
    Enemy* _enemy4;
    Enemy* _enemy5;

    bool _forward;
    bool _backward;

    bool _debug;

    bool _third;

    Vector3 origen;
    Vector3 destino;

    EasyCamera* _camera;

    // CIRCUIT
    Circuit* _circuit;
    Ogre::Vector3 _start;

    // OVERLAY
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay* _tiempoOverlay;
    OverlayElement* _OETiempo;
    Ogre::Overlay * _overlayVelocidad;
    Ogre::Overlay * _aguja;

    // TIMER
    EasyTimer* _timeLap;
    EasyTimer* _timeStart;

    // MUSIC
    TrackManager* _trackMgr;
    SoundFXManager* _soundFxMgr;
    TrackPtr _musicTrack;
    bool _engineSound;
    SoundFXPtr _engine;

};

#endif // PLAYSTATE_H
