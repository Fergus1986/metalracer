#include "OpcionesState.h"

OpcionesState::OpcionesState(String name): GameState(name){

}

OpcionesState::~OpcionesState()
{
}

String OpcionesState::getName()
{
    return this->_name;
}

void OpcionesState::enter()
{
    cout << "OpcionesMenu" << endl;
//    _root = Ogre::Root::getSingletonPtr();
//    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "OpcionesSceneManager");

//    _camera = new EasyCamera("OpcionesCamera", _sceneManager);

//    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
//    // Nuevo background colour.
//    _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

//    Real width = _viewport->getActualWidth();
//    Real height = _viewport->getActualHeight();
//    _camera->getCamera()->setAspectRatio(width/height);

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    createScene();
}

void OpcionesState::exit()
{
//    if(_sceneManager){
//        _sceneManager->destroyAllCameras();
//        _sceneManager->clearScene();
//        _root->destroySceneManager(_sceneManager);
//        _root->getAutoCreatedWindow()->removeAllViewports();
////        delete _sceneManager;
//    }
    _overlayOpciones->hide();

    cout << "OpcionesState exiting..." << endl;

}

void OpcionesState::pause()
{
}

void OpcionesState::resume()
{
}

void OpcionesState::createScene()
{
    _overlayOpciones= _overlayManager->getByName("Opciones");
    _overlayOpciones->show();
}

void OpcionesState::keyPressed(const OIS::KeyEvent &e)
{
}

void OpcionesState::keyReleased(const OIS::KeyEvent &e)
{
    if(e.key == OIS::KC_ESCAPE)
    {
        EasyOgre::getSingleton().popState();
    }
}

void OpcionesState::mouseMoved(const OIS::MouseEvent &e)
{
}

void OpcionesState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void OpcionesState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

bool OpcionesState::frameStarted(const FrameEvent &evt)
{
    return true;
}

bool OpcionesState::frameEnded(const FrameEvent &evt)
{
    return true;
}

CEGUI::MouseButton OpcionesState::convertMouseButton(OIS::MouseButtonID id)
{
}
