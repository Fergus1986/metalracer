#include "Random.h"

Random::Random(const int min, const int max)
{
    _number = min + (rand() % (int)(max - min + 1));
}

Random::~Random()
{
}

int Random::getNumber()
{
    cout << "random: " << _number << endl;
    return _number;
}
