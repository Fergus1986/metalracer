#ifndef RANDOM_H
#define RANDOM_H

#include <iostream>
#include <cstdlib>

using namespace std;

class Random
{
public:
    Random(const int min, const int max);
    ~Random();

    int getNumber();

private:

    int _number;


};

#endif // RANDOM_H
