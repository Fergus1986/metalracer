#ifndef DYNAMICARRAY_H
#define DYNAMICARRAY_H

#include <vector>

template <typename T>
class DynamicArray
{
public:
    DynamicArray(){};

    DynamicArray(int rows, int cols): dArray(rows, std::vector<T>(cols)){}

    std::vector<T> & operator[](int i)
    {
        return dArray[i];
    }
    const std::vector<T> & operator[] (int i) const
    {
        return dArray[i];
    }
    void resize(int rows, int cols)//resize the two dimentional array
    {
        dArray.resize(rows);
        for(int i = 0;i < rows;++i) dArray[i].resize(cols);
    }

private:
    std::vector<std::vector<T> > dArray;
};

#endif // DYNAMICARRAY_H
