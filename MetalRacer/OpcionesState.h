#ifndef OPCIONESSTATE_H
#define OPCIONESSTATE_H
#include "EasyOgre/GameState.h"
#include "EasyOgre/EasyOgre.h"
#include "EasyOgre/EasyCamera.h"
#include <CEGUI.h>

#include <iostream>

using namespace Ogre;
using namespace std;
class OpcionesState: public GameState
{
public:
    OpcionesState(Ogre::String name);
    ~OpcionesState();
    Ogre::String getName();

    void enter();
    void exit();
    void pause();
    void resume();


    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

private:
    void createScene();
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    Ogre::Viewport* _viewport;
    EasyCamera * _camera;
    // CEGUI
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    // OVERLAY
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayOpciones;
};

#endif // OPCIONESSTATE_H
