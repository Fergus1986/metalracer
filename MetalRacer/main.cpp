#include <iostream>

#include <OGRE/Ogre.h>
#include "EasyOgre/EasyOgre.h"


#include "PlayState.h"
#include "MenuState.h"
#include "LogrosState.h"
#include "AcercaState.h"
#include "OpcionesState.h"
#include "PauseState.h"
#include "TutorialState.h"
#include "PauseState.h"
#include "IntroState.h"

using namespace std;

int main()
{
    Ogre::String gameName = "MetalRacer";

    EasyOgre* ogreEngine = new EasyOgre(gameName);

    if(ogreEngine->loadResources("resources.cfg") && ogreEngine->configure() ){
//        EasyOgre::getSingletonPtr()->_debugLog->logMessage(gameName + " iniciado");

        MenuState* menu = new MenuState("MenuState");
        PlayState* play = new PlayState("PlayState");
        LogrosState* logros = new LogrosState("LogrosState");
        AcercaState* acerca = new AcercaState("AcercaState");
        OpcionesState* opciones = new OpcionesState("OpcionesState");
        TutorialState* tutorial = new TutorialState("TutorialState");
        PauseState* pausa = new PauseState("PauseState");
        IntroState* intro = new IntroState("IntroState");

        ogreEngine->addState(play);
        ogreEngine->addState(menu);
        ogreEngine->addState(logros);
        ogreEngine->addState(acerca);
        ogreEngine->addState(opciones);
        ogreEngine->addState(tutorial);
        ogreEngine->addState(pausa);
        ogreEngine->addState(intro);

        ogreEngine->addState(play);
        ogreEngine->addState(menu);

//        EasyOgre::getSingletonPtr()->_debugLog->logMessage("Estados creados");

        ogreEngine->start("IntroState");
    }

    cout << "Exito!!" << endl;
    return 0;
}

