#include "TutorialState.h"

TutorialState::TutorialState(String name): GameState(name)
{
}

TutorialState::~TutorialState()
{
}

String TutorialState::getName()
{
    return this->_name;
}

void TutorialState::enter()
{
    cout << "Tutorial Menu" << endl;
//    _root = Ogre::Root::getSingletonPtr();
//    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "TutorialSceneManager");

//    _camera = new EasyCamera("TutorialCamera", _sceneManager);

//    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
//    // Nuevo background colour.
//    _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

//    Real width = _viewport->getActualWidth();
//    Real height = _viewport->getActualHeight();
//    _camera->getCamera()->setAspectRatio(width/height);

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    createScene();
}

void TutorialState::exit()
{
    cout << "TutorialState exiting..." << endl;
//    if(_sceneManager){
//        _sceneManager->destroyAllCameras();
//        _sceneManager->clearScene();
//        _root->destroySceneManager(_sceneManager);
//        _root->getAutoCreatedWindow()->removeAllViewports();
////        delete _sceneManager;
//    }
    _overlayTutorial->hide();
}

void TutorialState::pause()
{
}

void TutorialState::resume()
{
}

void TutorialState::createScene()
{
    _overlayTutorial= _overlayManager->getByName("Tutorial");
    cout << "Overlay: " <<  _overlayTutorial->getName() << endl;
    _overlayTutorial->show();
}

void TutorialState::keyPressed(const OIS::KeyEvent &e)
{
}

void TutorialState::keyReleased(const OIS::KeyEvent &e)
{
    if(e.key == OIS::KC_ESCAPE)
    {
        EasyOgre::getSingleton().popState();
    }
}

void TutorialState::mouseMoved(const OIS::MouseEvent &e)
{
}

void TutorialState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void TutorialState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

bool TutorialState::frameStarted(const FrameEvent &evt)
{
    return true;
}

bool TutorialState::frameEnded(const FrameEvent &evt)
{
    return true;
}

CEGUI::MouseButton TutorialState::convertMouseButton(OIS::MouseButtonID id)
{
}
