#include "PlayState.h"


PlayState::PlayState(String name): GameState(name)
{
    _exitGame = false;
    _trackMgr =  EasyOgre::getSingletonPtr()->m_pAudioMgr;
    _soundFxMgr =  EasyOgre::getSingletonPtr()->m_pSoundMgr;
}

PlayState::~PlayState()
{
}

String PlayState::getName()
{
    return this->_name;
}

void PlayState::enter()
{
    cout << "Enter PlayState" << endl;

    _root = Ogre::Root::getSingletonPtr();

    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _camera = new EasyCamera("PlayCamera", _sceneManager);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

    Real width = _viewport->getActualWidth();
    Real height = _viewport->getActualHeight();
    _camera->getCamera()->setAspectRatio(width/height);
    _exitGame = false;
    _debug = false;
    _third = false;

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _tiempoOverlay = this->_overlayManager->getByName("Contador");
    _OETiempo = this->_overlayManager->getOverlayElement("time");
    _aguja = this->_overlayManager->getByName("Aguja");
    _overlayVelocidad = _overlayManager->getByName("Velocidad");
    _OETiempo->setCaption("Listo");

    createScene();
    createPhysics();
    createWorld();
}

void PlayState::exit()
{
    if(_sceneManager){
        _sceneManager->destroyAllCameras();
        _sceneManager->clearScene();
        _root->destroySceneManager(_sceneManager);
        _root->getAutoCreatedWindow()->removeAllViewports();
        //        delete _sceneManager;
    }

    _timeStart->resetearTiempo();
    _timeLap->resetearTiempo();

    hideGUI();

    this->_musicTrack->stop();
    this->_musicTrack->unload();

    // Eliminar mundo dinamico y debugDrawer -------------------------
    //    delete _world->getDebugDrawer();
    //    _world->setDebugDrawer(0);
    //    delete _world;
}

void PlayState::pause()
{
    this->_musicTrack->pause();
    _timeStart->pararTiempo();

    _timeLap->pararTiempo();

    hideGUI();
}

void PlayState::resume()
{
    this->_musicTrack->play();

    _timeLap->continuarTiempo();

    _timeStart->continuarTiempo();
    drawGUI();

}

void PlayState::createScene()
{
    SceneNode* node = _sceneManager->createSceneNode("LightNode");
    _sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    //    _sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_MODULATIVE);
    // Desert Ilumination
    _sceneManager->setAmbientLight(Ogre::ColourValue(0.922, 0.718, 0.302));
    Ogre::Light* light = _sceneManager->createLight("Light");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(1,-1,0));
    node->attachObject(light);

    _sceneManager->getRootSceneNode()->addChild(node);

    // TIMERS
    _timeLap = new EasyTimer(ADELANTE, 10,0);
    _timeLap->pararTiempo();
    _timeStart = new EasyTimer(ATRAS, 0, 4);
    _timeStart->pararTiempo();

    drawGUI();

}

void PlayState::createPhysics()
{
    // Bounds and gravity
    AxisAlignedBox worldBounds = AxisAlignedBox(
                Vector3 (-1000, -1000, -1000),
                Vector3 (1000,  1000,  1000));

    Vector3 gravity = Vector3(0, -9.8, 0);

    // World
    _physcisManager = new PhysicsManager(_sceneManager, worldBounds, gravity);
}

void PlayState::createWorld()
{
    SceneNode* nodoEscenario = _sceneManager->createSceneNode("Escenario");
    _sceneManager->getRootSceneNode()->addChild(nodoEscenario);
    CircuitManager* circuitManager = new CircuitManager (_sceneManager, _physcisManager);
    _circuit= circuitManager->loadCircuit("media/circuit/circuit.tmx");

    _enemy1 = new Enemy("enemigo", "enemigoMaterial1", Vector3(35, 1, 30), _circuit, _sceneManager, _physcisManager);
    _enemy2 = new Enemy("enemigo", "enemigoMaterial2", Vector3(45, 1, 30), _circuit, _sceneManager, _physcisManager);
    _enemy3 = new Enemy("enemigo", "enemigoMaterial3", Vector3(35, 1, 20), _circuit, _sceneManager, _physcisManager);
    _enemy4 = new Enemy("enemigo", "enemigoMaterial4", Vector3(45, 1, 20), _circuit, _sceneManager, _physcisManager);
    _enemy5 = new Enemy("enemigo", "enemigoMaterial5", Vector3(35, 1, 10), _circuit, _sceneManager, _physcisManager);

    cout << "Enemies created" << endl;

    _character = new Character("personaje", Vector3(45, 1, 10), _sceneManager, _physcisManager);

    _start = Ogre::Vector3(_circuit->getStart()->getX() * 20, 0, _circuit->getStart()->getZ()*20);

    _forward = false;
    _backward = false;
    _timeStart->continuarTiempo();

    _musicTrack = this->_trackMgr->load("Rossini - William Tell (Full Overture).mp3");
    _engineSound = false;

    _engineSound = false;
    _engine = this->_soundFxMgr->load("engine.wav");

    this->_musicTrack->fadeIn(128, -1, 1000, 614);



}

void PlayState::keyPressed(const OIS::KeyEvent &e)
{  

    switch(e.key){

//    case OIS::KC_F1:
//        _third = !_third;
//        break;

    case OIS::KC_TAB:
        _debug = !_debug;
        break;

    case OIS::KC_LEFT:
        _character->_vehicle->turnLeft();
        break;
    case OIS::KC_RIGHT:
        _character->_vehicle->turnRight();
        break;
    case OIS::KC_DOWN:
        _backward = true;
        break;
    case OIS::KC_UP:
        _forward = true;
        break;
    default:
        break;

    }
}

void PlayState::keyReleased(const OIS::KeyEvent &e)
{
    switch(e.key)
    {

    case OIS::KC_ESCAPE:
        EasyOgre::getSingleton().pushState("PauseState");
        break;

    case OIS::KC_P:
         EasyOgre::getSingleton().pushState("PauseState");

    case OIS::KC_LEFT:
        _character->_vehicle->restoreFromLeft();
        break;
    case OIS::KC_RIGHT:
        _character->_vehicle->restoreFromRight();
        break;
    case OIS::KC_DOWN:
        _backward = false;
        break;
    case OIS::KC_UP:
        _forward = false;
        break;
    default:
        break;

    }
}

void PlayState::mouseMoved(const OIS::MouseEvent &e)
{
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

bool PlayState::frameStarted(const Ogre::FrameEvent &evt)
{
    Ogre::Real deltaT = evt.timeSinceLastFrame;
    int fps = 1.0 / deltaT;

    _physcisManager->_world->stepSimulation(deltaT);


    if(_debug){
        _physcisManager->_world->setShowDebugShapes(true);
    }else{
        _physcisManager->_world->setShowDebugShapes(false);
    }

    if(_forward || _backward){

        if(_forward){
            _character->_vehicle->accelerate();
        }else{
            _character->_vehicle->back();
        }


    }else{
        _character->_vehicle->brake();
    }

    _character->getNode()->resetToInitialState();
    _character->getNode()->setPosition(_character->_vehicle->getNode()->getPosition());
    _character->getNode()->yaw(_character->_vehicle->getNode()->getOrientation().getYaw());

    if(_third){
        // Camera 3D
        _character->getNode()->translate(0, 3, -10, Node::TS_LOCAL);
    }else{
        // Camera Aerial
        _character->getNode()->translate(0, 30, -15, Node::TS_LOCAL);
    }

    origen = _character->getNode()->getPosition();
    destino = _character->_vehicle->getNode()->getPosition();

    _camera->update(origen, destino);

    if(_timeStart->getTiempoActivo()){
        // Wait until the game starts

    }else{
        _third = true;

        _character->_vehicle->update();
//        _timeLap->continuarTiempo();

        _enemy1->update();
        _enemy2->update();
        _enemy3->update();
        _enemy4->update();
        _enemy5->update();

    }


    if(_character->getNode()->getPosition().distance(_start) < 8){
        _timeLap->resetearTiempo();
        _timeLap->continuarTiempo();
    }


    _OETiempo->setCaption((_timeLap->getCadenaTiempo()));

//    cout <<"delta: " << deltaT << " FPS: " << fps << endl;
    //    cout << "Engine: " << _character->_vehicle->getEngineForce() << endl;

    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent &evt)
{

    return true;
}

void PlayState::drawGUI()
{
    _tiempoOverlay->show();
    _overlayVelocidad->show();
    _OETiempo->setCaption((_timeLap->getCadenaTiempo()));
    _aguja->show();
}

void PlayState::hideGUI()
{
    _tiempoOverlay->hide();
    _timeStart->pararTiempo();
    _timeLap->pararTiempo();
    _overlayVelocidad->hide();
    _aguja->hide();
}
