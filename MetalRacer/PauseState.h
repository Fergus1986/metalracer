#ifndef PAUSESTATE_H
#define PAUSESTATE_H

#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>
#include <iostream>
#include "EasyOgre/EasyOgre.h"
#include "EasyOgre/EasyCamera.h"
#include "EasyOgre/GameState.h"

using namespace Ogre;
class PauseState: public GameState
{
public:
    PauseState(Ogre::String name);
    ~PauseState();
    Ogre::String getName();

    void enter();
    void exit();
    void pause();
    void resume();



    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

private:
    void createScene();
    void crearPausa();
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    Ogre::Viewport* _viewport;
    EasyCamera * _camera;
    // CEGUI
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    CEGUI::System * _ceguiSystem;

    bool continuar(const CEGUI::EventArgs &e);
    bool salir(const CEGUI::EventArgs &e);
    CEGUI::Window* pauseButton;
    CEGUI::Window* menuButton;
    CEGUI::Window* backButton;
    CEGUI::Window * _ceguiWindow;
    // OVERLAY
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayPausa;
};

#endif // PAUSESTATE_H
