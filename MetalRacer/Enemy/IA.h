#ifndef IA_H
#define IA_H


#include <OGRE/Ogre.h>
#include <math.h>
#include <list>
#include <iterator>

#include "Circuit/Circuit.h"
#include "Circuit/TrackPoint.h"
#include "Vehicle/Vehicle.h"


using namespace std;


class IA
{
public:
    IA();
    IA(Vehicle* vehicle, Circuit* circuit, int maxEngine, int maxTurn);
    ~IA();

    void update();

private:

    std::list<TrackPoint *> _track;
    std::list<TrackPoint *>::iterator _nextI;

    int _maxEngine;
    int _maxTurn;

    // Vectors
    Ogre::Vector3 _movement;
    Ogre::Vector3 _orientation;

    // Points
    Ogre::Vector3 _origin;
    Ogre::Vector3 _destiny;
    TrackPoint* _nextPoint;

    // Angle
    Ogre::Degree _angle;
    int _angleInt;
    Ogre::Quaternion _quaternion;

    Vehicle* _vehicle;
    Circuit* _circuit;

};

#endif // IA_H
