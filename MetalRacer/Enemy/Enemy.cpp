#include "Enemy.h"

Enemy::Enemy()
{
}

Enemy::Enemy(String name,
             String material,
             Vector3 position,
             Circuit *circuit,
             SceneManager *sceneManager,
             PhysicsManager *physicsManager)
{
    _name = name;
    _material = material;
    _position = position;
    _circuit = circuit;
    _sceneManager = sceneManager;
    _physcisManager = physicsManager;

    _vehicle = new Vehicle(
                _name,
                _position,
                1500,
                Real(10.f),
                Real(4.4f),
                Real(2.3f),
                Real(250.f),
                Real(10.5f),
                _sceneManager,
                _physcisManager
                );

    Random * random = new Random(-5, 5);

    _ia = new IA(_vehicle, _circuit, 6500, random->getNumber());

    _vehicle->_wheelsNodeParent->setVisible(false);
    _vehicle->_chassisEntity->setMaterialName(_material);
}

Enemy::~Enemy()
{
}

void Enemy::update()
{

    _ia->update();
}

SceneNode *Enemy::getNode()
{
}
