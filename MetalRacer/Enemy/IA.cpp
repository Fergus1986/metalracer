#include "IA.h"

IA::IA()
{
}

IA::IA(Vehicle *vehicle, Circuit *circuit, int maxEngine, int maxTurn)
{
    _vehicle = vehicle;
    _circuit = circuit;

    _maxEngine = maxEngine;
    _maxTurn = maxTurn;

    _track = _circuit->getTrack();
    _nextI = _track.begin();

    _origin = Ogre::Vector3::ZERO;
    _destiny = Ogre::Vector3::ZERO;
    _movement = Ogre::Vector3::ZERO;

}

IA::~IA()
{
}

void IA::update()
{
    _nextPoint = *(_nextI);

    _origin = Ogre::Vector3(_vehicle->getNode()->getPosition().x,
                            0,
                            _vehicle->getNode()->getPosition().z);


    _destiny = Ogre::Vector3(((_nextPoint->getX()*20) + _maxTurn),
                             0,
                             ((_nextPoint->getZ()*20) + _maxTurn));

    _movement = _destiny - _origin;

    _orientation = _vehicle->getNode()->getOrientation() * Vector3::UNIT_Z;
    _quaternion = _orientation.getRotationTo(_movement);
    _angle = _quaternion.getYaw().valueDegrees()/6;

    _angleInt = int(_angle.valueDegrees());

//    cout << "a: " << _angle << "angulo: " << angulo << endl;

    if (abs(_angleInt) > abs(_maxTurn)){

        if(_angleInt > 0){
            _vehicle->restoreFromRight();
            _vehicle->turnLeft();
        }else{
            _vehicle->restoreFromLeft();
            _vehicle->turnRight();
        }

    }else{
        _vehicle->restoreFromLeft();
        _vehicle->restoreFromRight();
    }


    if(_vehicle->getEngineForce() < _maxEngine){
        _vehicle->accelerate();
    }

    _vehicle->update();


    if (_movement.length() < 15){

//        cout << "n(" << _nextPoint->getX() << "," << _nextPoint->getZ() << ")" << endl;

        if((++_nextI) != _track.end()){

        }else{
            cout << "Fin" << endl;
            _nextI = _track.begin();

        }
    }
}
