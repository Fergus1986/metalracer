#ifndef ENEMY_H
#define ENEMY_H

#include "Vehicle/Vehicle.h"
#include "EasyOgre/PhysicsManager.h"

#include "Circuit/Circuit.h"
#include "Enemy/IA.h"

#include <string>

#include "Util/Random.h"


#include <OGRE/Ogre.h>

class Enemy
{
public:
    Enemy();
    Enemy(String name,
          String material,
          Vector3 position,
          Circuit* circuit,
          SceneManager* sceneManager,
          PhysicsManager* physicsManager);

    ~Enemy();

    void update();

    SceneNode* getNode();

    Ogre::String _name;
    Ogre::String _material;
    Ogre::Vector3 _position;
    SceneNode* _node;
    Vehicle *_vehicle;
    Circuit* _circuit;
    IA *_ia;

    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physcisManager;
};

#endif // ENEMY_H
