#ifndef CHARACTER_H
#define CHARACTER_H

#include "Vehicle/Vehicle.h"
#include "EasyOgre/PhysicsManager.h"

#include <string>


#include <OGRE/Ogre.h>

using namespace std;

class Character
{
public:
    Character();
    Character(String name,
              Vector3 position,
              SceneManager* sceneManager,
              PhysicsManager* physicsManager);

    ~Character();

    void update();

    SceneNode* getNode();

    string _name;
    Ogre::Vector3 _position;
    SceneNode* _node;
    Vehicle *_vehicle;

    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physcisManager;


};

#endif // CHARACTER_H
