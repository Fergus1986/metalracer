#include "Character.h"

Character::Character()
{
}

Character::Character(String name,
                     Ogre::Vector3 position,
                     SceneManager* sceneManager,
                     PhysicsManager* physicsManager)
{
    _name = name;
    _position = position; 
    _sceneManager = sceneManager;
    _physcisManager = physicsManager;

    _vehicle = new Vehicle(
                _name,
                _position,
                1500,
                Real(10.f),
                Real(4.4f),
                Real(2.3f),
                Real(250.f),
                Real(10.5f),
                _sceneManager,
                _physcisManager
                );

    _node = _sceneManager->createSceneNode();

}

Character::~Character()
{
}

void Character::update()
{
}


SceneNode *Character::getNode()
{
    return _node;
}
