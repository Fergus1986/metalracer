#include "Vehicle/Vehicle.h"

Vehicle::Vehicle()
{
}

Vehicle::Vehicle(const String name,
                 const Vector3 position,
                 const Real mass,
                 const Real SuspensionStiffness,
                 const Real SuspensionCompression,
                 const Real SuspensionDamping,
                 const Real MaxSuspensionTravelCm,
                 const Real FrictionSlip,
                 SceneManager *sceneManager,
                 PhysicsManager *physicsManager)
{

    _name = name;
    _position = position;
    _wheels = 4;
    _mass = mass;
    _suspensionStiffness = SuspensionStiffness;
    _suspensionCompression = SuspensionCompression;
    _suspensionDamping = SuspensionDamping;
    _maxSuspensionTravelCm = MaxSuspensionTravelCm;
    _frictionSlip = FrictionSlip;

    _engineForce = 0;
    _acceleration = 500;
    _maxEngineForce = 7000;
    _nitroForce = 1000;
    _steeringIncrement = 0.02f;
    _steeringClamp = 0.7f;

    _wheelRadius = 0.5f;
    _wheelWidth = 0.5f;
    _wheelFriction = 1e30f;

    _rollInfluence = 0.1f;

    _suspensionRestLenght = 0.6;


    _sceneManager = sceneManager;
    _physcisManager = physicsManager;

    createVehicle();
    initialState();

}

Vehicle::~Vehicle()
{
}

SceneNode *Vehicle::getNode()
{
    return this->_vehicleNode;
}

Real Vehicle::getEngineForce()
{
    return this->_engineForce;
}

void Vehicle::createVehicle()
{
    const Ogre::Vector3 chassisShift(0, 1.1, 0);

    // Chassis & Vehicle Nodes
    _vehicleNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _chassisNode = _vehicleNode->createChildSceneNode();

    _chassisEntity = _sceneManager->createEntity(_name + ".mesh");
    _chassisNode->attachObject(_chassisEntity);
    _chassisNode->setPosition(chassisShift);

    // Chassis & Vehicle Shapes
//    _chassisShape = new BoxCollisionShape(Ogre::Vector3(0.9f,0.6f,2.0f));
//    OgreBulletCollisions::StaticMeshToShapeConverter *_converter = new OgreBulletCollisions:: StaticMeshToShapeConverter(_chassisEntity);
//    OgreBulletCollisions::CollisionShape* _shape = _converter->createConvex();
//    OgreBulletCollisions::CollisionShape* _shape = _converter->createBox();

    AxisAlignedBox boundingB = _chassisEntity->getBoundingBox();
    Vector3 size = boundingB.getSize();
    size /= 2.5;   // El tamano en Bullet se indica desde el centro
    _chassisShape = new OgreBulletCollisions::BoxCollisionShape(size);

//    _chassisShape = _shape;
    _chassisNode->setPosition(chassisShift);
    _vehicleShape = new CompoundCollisionShape();
    _vehicleShape->addChildShape(_chassisShape, chassisShift);

    // Vehicle Body
    StringStream ss;
    ss << _name;
    ss << _position.x << "_" << _position.z;
    _vehicleBody = new WheeledRigidBody(ss.str(), _physcisManager->getWorld());
    _vehicleBody->setShape(_vehicleNode, _vehicleShape, 0.7, 0.4, _mass,_position);
    _vehicleBody->setDamping(0.2,0.2);
    _vehicleBody->disableDeactivation();

    // Vehicle Tuning
    _vehicleTuning =  new VehicleTuning (
                _suspensionStiffness,
                _suspensionCompression,
                _suspensionDamping,
                _maxSuspensionTravelCm,
                _frictionSlip);

    // Vehicle
    _vehicleRayCaster = new VehicleRayCaster(_physcisManager->getWorld());
    _vehicle = new RaycastVehicle(_vehicleBody, _vehicleTuning, _vehicleRayCaster);
    _vehicle->setCoordinateSystem(0,1,2);

    // Wheels
    Ogre::Vector3 wheelDirectionCS0(0,-1,0);
    Ogre::Vector3 wheelAxleCS(-1,0,0);

    _wheelEntities.reserve(_wheels);
    _wheelNodes.reserve(_wheels);

    _wheelsNodeParent = _sceneManager->getRootSceneNode()->createChildSceneNode();

    for (size_t i = 0; i <_wheels; i++) {
        _wheelEntities[i] = _sceneManager->createEntity(_name + "_rueda.mesh");
        _wheelNodes[i] = _wheelsNodeParent->createChildSceneNode();
        _wheelNodes[i]->attachObject(_wheelEntities[i]);
        _wheelNodes[i]->scale(1.3, 1.3, 1.3);
    }

    float connectionHeight = 0.7f;

    bool isFrontWheel = true;
    Ogre::Vector3 connectionPointCS0 (1-(0.3*_wheelWidth),
                                      connectionHeight, 2-_wheelRadius);

    _vehicle->addWheel(_wheelNodes[0],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);

    connectionPointCS0 = Ogre::Vector3(-1+(0.3*_wheelWidth),
                                       connectionHeight, 2-_wheelRadius);

    _vehicle->addWheel(_wheelNodes[1],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);

    isFrontWheel = false;
    connectionPointCS0 = Ogre::Vector3(-1+(0.3*_wheelWidth),
                                       connectionHeight,-2+_wheelRadius);

    _vehicle->addWheel(_wheelNodes[2],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);


    connectionPointCS0 = Ogre::Vector3(1-(0.3*_wheelWidth),
                                       connectionHeight,-2+_wheelRadius);

    _vehicle->addWheel(_wheelNodes[3],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);

    cout << "Vehicle " << _name << " created" << endl;
}

void Vehicle::initialState()
{
    _steeringLeft = false;
    _steeringRight = false;
    _steering = 0;
    _engineForce = 0;
    cout << "Vehicle " << _name << " initialState" << endl;
}

void Vehicle::accelerate()
{

    if(_engineForce <= _maxEngineForce){
        _engineForce += _acceleration;
    }
}

void Vehicle::back()
{
    if(_engineForce > -_maxEngineForce/2){
        _engineForce -= _acceleration;
    }
}

void Vehicle::brake()
{
    if(_engineForce != 0){
        _engineForce = 0;
    }

}

void Vehicle::turnLeft()
{
    _steeringLeft = true;
}

void Vehicle::restoreFromLeft()
{
    _steeringLeft = false;
}

void Vehicle::restoreFromRight()
{
    _steeringRight = false;
}

void Vehicle::turnRight()
{
    _steeringRight = true;
}

void Vehicle::update()
{
    // Apply engine Force on relevant wheels
    _vehicle->applyEngineForce(_engineForce, 0);
    _vehicle->applyEngineForce(_engineForce, 1);

    // Calculate Steering
    if (_steeringLeft)
    {
        _steering += _steeringIncrement;
        if (_steering > _steeringClamp)
            _steering = _steeringClamp;
    }

    if (_steeringRight)
    {
        _steering -= _steeringIncrement;
        if (_steering < -_steeringClamp)
            _steering = -_steeringClamp;
    }

    if(!_steeringLeft && !_steeringRight){
        // mSteering = 0;         // Arcade 100%

        // Arcade 50%
        if(_steering < 0){
            // Right
            _steering += _steeringIncrement;

            // Center
            if(_steering > 0){
                _steering =0;
            }

        }else{
            // Left
            _steering -= _steeringIncrement;

            // Center
            if(_steering < 0){
                _steering =0;
            }

        }
    }

    // Apply Steering on relevant wheels
    _vehicle->setSteeringValue(_steering, 0);
    _vehicle->setSteeringValue(_steering, 1);
}

#include "Vehicle.h"


