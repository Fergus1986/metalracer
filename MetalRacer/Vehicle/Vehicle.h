#ifndef VEHICLE_H
#define VEHICLE_H

#include <OGRE/Ogre.h>
#include <vector>

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>
#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>

#include "EasyOgre/PhysicsManager.h"

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;
using namespace std;

class Vehicle
{
public:
    Vehicle();
    Vehicle(const String name,
            const Vector3 position,
            const Real mass,
            const Real SuspensionStiffness,
            const Real SuspensionCompression,
            const Real SuspensionDamping,
            const Real MaxSuspensionTravelCm,
            const Real FrictionSlip,
            SceneManager* sceneManager,
            PhysicsManager* physicsManager);

    ~Vehicle();

    SceneNode* getNode();
    Real getEngineForce();


    void initialState();
    void accelerate();
    void back();
    void brake();
    void turnLeft();
    void restoreFromLeft();
    void restoreFromRight();
    void turnRight();

    void update();

//private:
    void createVehicle();
    void addWheel();

    Ogre::String _name;
    Ogre::Vector3 _position;

    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physcisManager;

    // Vehicle characteristics
    int _mass;
    int _acceleration;  // makes the car faster
    int _maxEngineForce; // max power
    int _nitroForce;
    Real _steeringIncrement;  // makes the wheels turn a little slower to avoid sudden 'flipping'
    Real _steeringClamp; // the max wheel turning angle

    Real _wheelRadius; // size of the wheel (height)
    Real _wheelWidth; // wheel width, we wanted this wider for more 'race car' handling
    Real _wheelFriction; // how much friction the wheel has

    Real _rollInfluence; // how likely the car is to roll

    Real _suspensionStiffness; // The stiffness constant for the suspension.  10.0 - Offroad buggy, 50.0 - Sports car, 200.0 - F1 Car
    Real _suspensionCompression; // how much the suspension will compress when it lands
    Real _suspensionDamping; // how much damping occurs when hitting bumps and jumps
    Real _suspensionRestLenght; // The maximum length of the suspension (metres)
    Real _maxSuspensionTravelCm; // The maximum distance the suspension can be compressed (centimetres)
    Real _frictionSlip; // how much friction before it slips

    // Chassis
    Ogre::Entity* _chassisEntity;
    Ogre::SceneNode* _chassisNode;
    OgreBulletCollisions::CollisionShape* _chassisShape;

    // Vehicle
    Ogre::SceneNode* _vehicleNode;
    CompoundCollisionShape* _vehicleShape;
    OgreBulletDynamics::WheeledRigidBody* _vehicleBody;
    OgreBulletDynamics::VehicleTuning* _vehicleTuning;
    OgreBulletDynamics::VehicleRayCaster* _vehicleRayCaster;
    OgreBulletDynamics::RaycastVehicle* _vehicle;

    // Wheels
    int _wheels;
    Ogre::SceneNode* _wheelsNodeParent;
    std::vector<Ogre::Entity*> _wheelEntities;
    std::vector<Ogre::SceneNode*> _wheelNodes;
//    std::vector<Ogre::SceneNode*> _pruebaNodes;
//    std::vector<Ogre::Entity*> _pruebaEntidades;

    //Driving
    bool _steeringLeft;
    bool _steeringRight;
    Ogre::Real _steering;
    Ogre::Real _engineForce;

};

#endif // VEHICLE_H
