#ifndef LOGROSSTATE_H
#define LOGROSSTATE_H
#include <iostream>
#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "EasyOgre/EasyOgre.h"
#include "EasyOgre/GameState.h"
#include "EasyOgre/EasyCamera.h"



using namespace Ogre;
using namespace std;

using namespace Ogre;
class LogrosState: public GameState
{
public:
    LogrosState(Ogre::String name);
    ~LogrosState();
    Ogre::String getName();

    void enter();
    void exit();
    void pause();
    void resume();

    void createScene();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);


private:

    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    Ogre::Viewport* _viewport;
    EasyCamera * _camera;
    // CEGUI
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    // OVERLAY
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayLogros;
    OverlayElement* overlayLogros,*OLogros;
};

#endif // LOGROSSTATE_H
