TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = MetalRacer

INCLUDEPATH += /usr/include/SDL/
INCLUDEPATH += /usr/local/include/OGRE/
INCLUDEPATH += /usr/include/OIS/
INCLUDEPATH += /usr/local/include/bullet/
INCLUDEPATH += /usr/local/include/OgreBullet/
INCLUDEPATH += /usr/local/include/CEGUI
INCLUDEPATH += /usr/include/xercesc
INCLUDEPATH += /usr/include/glib-2.0
INCLUDEPATH += Sound/

CONFIG += link_pkgconfig
PKGCONFIG += gl SDL_mixer OGRE OIS CEGUI bullet OgreBullet xerces-c glib-2.0

LIBS += -lstdc++ -lCEGUIBase -lCEGUIOgreRenderer -lpthread

SOURCES += main.cpp \
    EasyOgre/EasyOgre.cpp \
    PlayState.cpp \
    EasyOgre/OISManager.cpp \
    EasyOgre/PhysicsManager.cpp \
    EasyOgre/Object3D.cpp \
    Vehicle/Vehicle.cpp \
    Vehicle/VehicleManager.cpp \
    Circuit/Square.cpp \
    Circuit/Circuit.cpp \
    Circuit/CircuitManager.cpp \
    Circuit/SquareType.cpp \
    EasyOgre/EasyCamera.cpp \
    Circuit/TrackPoint.cpp \
    Character/Character.cpp \
    Enemy/Enemy.cpp \
    Enemy/IA.cpp \
    MenuState.cpp \
    EasyOgre/EasyTimer.cpp \
    Sound/TrackManager.cpp \
    Sound/Track.cpp \
    Sound/SoundFXManager.cpp \
    Sound/SoundFX.cpp \
    TutorialState.cpp \
    PauseState.cpp \
    OpcionesState.cpp \
    LogrosState.cpp \
    IntroState.cpp \
    AcercaState.cpp \
    Util/Random.cpp

HEADERS += \
    EasyOgre/EasyOgre.h \
    EasyOgre/GameState.h \
    PlayState.h \
    EasyOgre/OISManager.h \
    EasyOgre/PhysicsManager.h \
    EasyOgre/Object3D.h \
    Vehicle/Vehicle.h \
    Vehicle/VehicleManager.h \
    Circuit/Square.h \
    Circuit/Circuit.h \
    Circuit/CircuitManager.h \
    Circuit/SquareType.h \
    EasyOgre/EasyCamera.h \
    Circuit/TrackPoint.h \
    Util/DynamicArray.h \
    Character/Character.h \
    Enemy/Enemy.h \
    Enemy/IA.h \
    MenuState.h \
    EasyOgre/EasyTimer.h \
    Sound/TrackManager.hpp \
    Sound/Track.hpp \
    Sound/SoundFXManager.hpp \
    Sound/SoundFX.hpp \
    TutorialState.h \
    PauseState.h \
    OpcionesState.h \
    LogrosState.h \
    IntroState.h \
    AcercaState.h \
    Util/Random.h

