#include "MenuState.h"

MenuState::MenuState(String name): GameState(name)
{
    _exitGame = false;
    _trackMgr =  EasyOgre::getSingletonPtr()->m_pAudioMgr;
    _soundFxMgr =  EasyOgre::getSingletonPtr()->m_pSoundMgr;
}

MenuState::~MenuState()
{
}

String MenuState::getName()
{
    return this->_name;
}

void MenuState::enter()
{
    cout << "Enter Menu" << endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "MenuSceneManager");

    _camera = new EasyCamera("MenuCamera", _sceneManager);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

    Real width = _viewport->getActualWidth();
    Real height = _viewport->getActualHeight();
    _camera->getCamera()->setAspectRatio(width/height);
    _exitGame = false;

    // CEGUI
    _ceguiSystem = CEGUI::System::getSingletonPtr();

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    createScene();

}

void MenuState::exit()
{
    cout << "MenuState exiting..." << endl;
    if(_sceneManager){
        _sceneManager->destroyAllCameras();
        _sceneManager->clearScene();
        _root->destroySceneManager(_sceneManager);
        _root->getAutoCreatedWindow()->removeAllViewports();
        //        delete _sceneManager;
    }

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();

    _overlayMenu->hide();

    this->_musicTrack->stop();
    this->_musicTrack->unload();

}

void MenuState::pause()
{
    hideGUI();

}

void MenuState::resume()
{
    drawGUI();
}

void MenuState::createScene()
{
    cout << "Crear Escena" << endl;

    _overlayMenu = _overlayManager->getByName("Menu");
    cout << "Overlay: " <<  _overlayMenu->getName() << endl;
//    _overlayMenu->show();

    _ceguiWindow = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","menuWindow");
    CEGUI::SchemeManager::getSingleton().create("Menu.scheme");
    CEGUI::System::getSingleton().setGUISheet(_ceguiWindow);


    _musicTrack = this->_trackMgr->load("Rossini - William Tell (Full Overture).mp3");
    this->_musicTrack->fadeIn(128, -1, 1000, 451);

    crearMenu();
    drawGUI();
}

void MenuState::keyPressed(const OIS::KeyEvent &e)
{
    if(e.key == OIS::KC_RETURN)
    {
        EasyOgre::getSingleton().changeState("PlayState");
    }
}

void MenuState::keyReleased(const OIS::KeyEvent &e)
{
    if(e.key == OIS::KC_ESCAPE)
    {
        _exitGame = true;
    }
}

void MenuState::mouseMoved(const OIS::MouseEvent &e)
{
    _ceguiSystem->injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel
//    if (e.state.Z.rel) _ceguiSystem->injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void MenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}

void MenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}

bool MenuState::frameStarted(const FrameEvent &evt)
{
    return true;
}

bool MenuState::frameEnded(const FrameEvent &evt)
{
    if (_exitGame)
        return false;
    return true;
}

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void MenuState::crearMenu()
{
    //Boton para Comenzar
    startButton = CEGUI::WindowManager::getSingleton().createWindow("Menu/BotonJugar","StartButton");
    startButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    startButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.16,0),CEGUI::UDim(0.4,0)));

    //Boton para Puntuaciones
    logrosButton = CEGUI::WindowManager::getSingleton().createWindow("Menu/BotonLogros","LogrosButton");
    logrosButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    logrosButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.16,0),CEGUI::UDim(0.5,0)));
    //Boton para Creditos
    creditsButton = CEGUI::WindowManager::getSingleton().createWindow("Menu/BotonCreditos","CreditsButton");
    creditsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.16,0),CEGUI::UDim(0.6,0)));
    //Boton para el tutorial

    tutorialButton = CEGUI::WindowManager::getSingleton().createWindow("Menu/BotonTutorial","TutorialButton");

    tutorialButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    tutorialButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.5,0)));

    //Quit button
    quitButton = CEGUI::WindowManager::getSingleton().createWindow("Menu/BotonSalir","QuitButton");
    quitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.6,0)));

    //options button

    optionsButton = CEGUI::WindowManager::getSingleton().createWindow("Menu/BotonOpciones","OptionsButton");
    optionsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    optionsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.4,0)));

    //Suscribimos cada boton a un evento que controlara que hace si pulsas en el
    startButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::empezarJuego, this));
    logrosButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarLogros, this));
    creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarAcerca, this));
    tutorialButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarTutorial, this));
    quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::salir, this));
    optionsButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarOpciones,this));

    //Añadimos los botones a la ventana
    _ceguiWindow->addChildWindow(startButton);
    _ceguiWindow->addChildWindow(logrosButton);
    _ceguiWindow->addChildWindow(creditsButton);
    _ceguiWindow->addChildWindow(tutorialButton);
    _ceguiWindow->addChildWindow(quitButton);
    _ceguiWindow->addChildWindow(optionsButton);
}

void MenuState::drawGUI()
{
    _overlayMenu->show();
    startButton->show();
    logrosButton->show();
    creditsButton->show();
    tutorialButton->show();
    optionsButton->show();
    quitButton->show();

    CEGUI::MouseCursor::getSingleton().show();
}

void MenuState::hideGUI()
{
    _overlayMenu->hide();
    startButton->hide();
    logrosButton->hide();
    creditsButton->hide();
    tutorialButton->hide();
    optionsButton->hide();
    quitButton->hide();

    CEGUI::MouseCursor::getSingleton().hide();
}

bool MenuState::empezarJuego(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().changeState("PlayState");
    return true;
}

bool MenuState::mostrarLogros(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().pushState("LogrosState");
    return true;
}

bool MenuState::mostrarAcerca(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().pushState("AcercaState");
    return true;
}

bool MenuState::mostrarTutorial(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().pushState("TutorialState");
    return true;
}

bool MenuState::mostrarOpciones(const CEGUI::EventArgs &e)
{
    EasyOgre::getSingleton().pushState("OpcionesState");
    return true;
}

bool MenuState::salir(const CEGUI::EventArgs &e)
{
    _exitGame=true;
    return true;
}
