#ifndef MENUSTATE_H
#define MENUSTATE_H

#include <iostream>

#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "EasyOgre/EasyOgre.h"
#include "EasyOgre/GameState.h"
#include "EasyOgre/EasyCamera.h"



using namespace Ogre;
using namespace std;

class MenuState: public GameState
{
public:
    MenuState(Ogre::String name);
    ~MenuState();

    Ogre::String getName();

    void enter();
    void exit();
    void pause();
    void resume();

    void createScene();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

    // CEGUI
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

    void crearMenu();
    void drawGUI();
    void hideGUI();
    bool empezarJuego(const CEGUI::EventArgs &e);
    bool mostrarLogros(const CEGUI::EventArgs &e);
    bool mostrarAcerca(const CEGUI::EventArgs &e);
    bool mostrarTutorial(const CEGUI::EventArgs &e);
    bool mostrarOpciones(const CEGUI::EventArgs &e);
    bool salir(const CEGUI::EventArgs &e);


private:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    Ogre::Viewport* _viewport;
    EasyCamera * _camera;

    // CEGUI
    CEGUI::OgreRenderer* _ceguiRenderer;
    CEGUI::System * _ceguiSystem;
    CEGUI::Window * _ceguiWindow;

    CEGUI::Window *startButton;
    CEGUI::Window *logrosButton;
    CEGUI::Window *optionsButton;
    CEGUI::Window *creditsButton;
    CEGUI::Window *tutorialButton;
    CEGUI::Window *quitButton;

    // OVERLAY
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayMenu;

    bool _exitGame;

    // MUSIC
    TrackManager*				_trackMgr;
    SoundFXManager*				_soundFxMgr;
    TrackPtr 					_musicTrack;


};

#endif // MENUSTATE_H
