#include "AcercaState.h"

AcercaState::AcercaState(String name): GameState(name)
{
}

AcercaState::~AcercaState()
{
}

String AcercaState::getName()
{
    return this->_name;
}

void AcercaState::enter()
{
    cout << "Acerca Menu" << endl;
//    _root = Ogre::Root::getSingletonPtr();
//    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "AcercaSceneManager");

//    _camera = new EasyCamera("AcercaCamera", _sceneManager);

//    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera->getCamera());
//    // Nuevo background colour.
//    _viewport->setBackgroundColour(Ogre::ColourValue(0.3,0.3,0.3));

//    Real width = _viewport->getActualWidth();
//    Real height = _viewport->getActualHeight();
//    _camera->getCamera()->setAspectRatio(width/height);


    // CEGUI
    //  _ceguiSystem = CEGUI::System::getSingletonPtr();

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    createScene();

}

void AcercaState::exit()
{
    cout << "AcercaState exiting..." << endl;
//    if(_sceneManager){
//        _sceneManager->destroyAllCameras();
//        _sceneManager->clearScene();
//        _root->destroySceneManager(_sceneManager);
//        _root->getAutoCreatedWindow()->removeAllViewports();
//        //        delete _sceneManager;
//    }

//    CEGUI::WindowManager::getSingleton().destroyAllWindows();
//    CEGUI::MouseCursor::getSingleton().hide();
    _overlayAcerca->hide();
}

void AcercaState::pause()
{
}

void AcercaState::resume()
{
}

void AcercaState::createScene()
{
    _overlayAcerca = _overlayManager->getByName("Acerca");
    cout << "Overlay: " <<  _overlayAcerca->getName() << endl;
    _overlayAcerca->show();
    // CEGUI::MouseCursor::getSingleton().show();
}

void AcercaState::keyPressed(const OIS::KeyEvent &e)
{
}

void AcercaState::keyReleased(const OIS::KeyEvent &e)
{
    if(e.key == OIS::KC_ESCAPE)
    {
        EasyOgre::getSingleton().popState();
    }
}

void AcercaState::mouseMoved(const OIS::MouseEvent &e)
{
//    cout << "raton movido en acerca" << endl;
//    _ceguiSystem->injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel
    //    if (e.state.Z.rel) _ceguiSystem->injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void AcercaState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}

void AcercaState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}

bool AcercaState::frameStarted(const FrameEvent &evt)
{
    return true;
}

bool AcercaState::frameEnded(const FrameEvent &evt)
{
    return true;
}


CEGUI::MouseButton AcercaState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}
