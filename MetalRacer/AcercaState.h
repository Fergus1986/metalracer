#ifndef ACERCASTATE_H
#define ACERCASTATE_H
#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>
#include "EasyOgre/GameState.h"
#include "EasyOgre/EasyOgre.h"
#include "EasyOgre/EasyCamera.h"

#include <iostream>

using namespace Ogre;
using namespace std;
class AcercaState: public GameState
{
public:
    AcercaState(Ogre::String name);
    ~AcercaState();
    Ogre::String getName();
    void enter();
    void exit();
    void pause();
    void resume();

    void createScene();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

private:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    Ogre::Viewport* _viewport;
    EasyCamera * _camera;
    // CEGUI
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    CEGUI::System * _ceguiSystem;
    CEGUI::Window * _ceguiWindow;
    CEGUI::Window* acercaW;
    CEGUI::Window* backButton;
    // OVERLAY
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayAcerca;


};

#endif // ACERCASTATE_H
